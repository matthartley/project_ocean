#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/keysymdef.h>
#include <GL/glew.h>
#include <GL/glx.h>

#include "linux.h"
#include "renderer.h"
#include "game.h"

#include "renderer.c"
#include "game.c"

int main ()
{
	display = XOpenDisplay(NULL);
	if (display == NULL)
	{
		printf("Cannot open X display\n");
		return 1;
	}

	root = DefaultRootWindow(display);

	visual_info = glXChooseVisual(display, 0, attributes);
	if (visual_info == NULL)
	{
		printf("Visual info not found\n");
	}
	else
	{
		printf("Visual: %p\n", (void*)visual_info->visualid);
	}

	color_map = XCreateColormap(display, root, visual_info->visual, AllocNone);
	set_window_attributes.colormap = color_map;
	set_window_attributes.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask;

	window = XCreateWindow(display, root, 0, 0, 800, 600, 0,
		visual_info->depth, InputOutput, visual_info->visual,
		CWColormap | CWEventMask, &set_window_attributes);

	XMapWindow(display, window);
	XStoreName(display, window, "Xlib window");

	gl_context = glXCreateContext(display, visual_info, NULL, GL_TRUE);
	glXMakeCurrent(display, window, gl_context);

	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		printf("glewInit failed\n");
	}
	else
	{
		printf("glewInit\n");
	}

	renderer_init();
	game_init();

	while (running)
	{
		//printf("window %i\n", window);
		while (XPending(display) > 0)
		{
			XNextEvent(display, &event);

			if (event.type == Expose)
			{
				//printf("Expose event\n");
			}
			else if (event.type == KeyPress)
			{
				set_key(XKeycodeToKeysym(display, event.xkey.keycode, 0), 1);
				//printf("KeyPress: %u %s\n", event.xkey.keycode, XKeysymToString(XKeycodeToKeysym(display, event.xkey.keycode, 0)));
			}
			else if (event.type == KeyRelease)
			{
				set_key(XKeycodeToKeysym(display, event.xkey.keycode, 0), 0);
				//printf("KeyRelease: %u %i\n", event.xkey.keycode, XKeysymToString(XKeycodeToKeysym(display, event.xkey.keycode, 0)));
				if (XKeycodeToKeysym(display, event.xkey.keycode, 0) == XK_Escape)
				{
					running = 0;
				}
			}
			else if (event.type == ClientMessage)
			{
				// Do we need to handle any client messages?
			}
		}

		//XGetWindowAttributes(display, window, &window_attributes);
		game_update_and_render();
		glXSwapBuffers(display, window);
	}

	XDestroyWindow(display, window);
	XCloseDisplay(display);
	printf("done\n");
	return 0;
}

void set_key (int keysym, int state)
{
	// Please remember the break; very important
	switch (keysym)
	{
		case XK_Up:
		{
			game_input.key_up = state;
		}
		break;
		case XK_Right:
		{
			game_input.key_right = state;
		}
		break;
		case XK_Down:
		{
			game_input.key_down = state;
		}
		break;
		case XK_Left:
		{
			game_input.key_left = state;
		}
		break;
	}
}