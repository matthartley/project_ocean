#ifndef _GAME_HEADER
#define _GAME_HEADER

void game_init ();
void game_update_and_render ();

struct renderer_buffer game_test_buffer;

struct game_state
{
	int player_x;
	int player_y;
};

struct game_input
{
	int key_up;
	int key_right;
	int key_down;
	int key_left;
};

struct game_state game = {};
struct game_input game_input = {};

#endif