#ifndef _WIN32_HEADER
#define _WIN32_HEADER

int CALLBACK WinMain (HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow);

LRESULT CALLBACK window_callback (HWND hwnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam);

void update_and_render ();
void init ();

HDC hdc;
HGLRC gl_context;
HPALETTE hpalette;
bool running;

//typedef void GLGENBUFFERS(GLsizei size, GLuint *buffers)
//static GLGENBUFFERS *gl_gen_buffers;

#endif