#include "game.h"

void game_init ()
{
	game_test_buffer = renderer_create_rect(40, 40, 0.0, 1.0, 1.0, 1.0);
}

void game_update_and_render ()
{
	glClear(GL_COLOR_BUFFER_BIT);

	renderer_draw_rect_buffer(game_test_buffer, 100, 100,
		1.0, 1.0, 0.0, 1.0);

	// player update
	if (game_input.key_right) {
		game.player_x += 2;
	}
	if (game_input.key_left) {
		game.player_x -= 2;
	}
	if (game_input.key_down) {
		game.player_y += 2;
	}
	if (game_input.key_up) {
		game.player_y -= 2;
	}
	renderer_draw_rect(game.player_x, game.player_y, 20, 20,
		1.0, 0.0, 1.0, 1.0);
}