#include <stdio.h>
#include <windows.h>
//#include <GL/gl.h>
#include "../lib/GL/glew.h"

#include "win32.h"

int CALLBACK WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	WNDCLASS window = {};

	window.style = CS_OWNDC|CS_HREDRAW|CS_VREDRAW;
	window.lpfnWndProc = window_callback;
	window.hInstance = hInstance;
	window.lpszClassName = "Win32 window class";

	RECT window_rect;
	window_rect.left = 0;
	window_rect.right = 800;
	window_rect.top = 0;
	window_rect.bottom = 600;

	AdjustWindowRectEx(&window_rect, WS_OVERLAPPEDWINDOW|WS_VISIBLE, FALSE, 0);
	// printf("%i %i %i %i \n", (int)window_rect.left, (int)window_rect.right,
	// 	(int)window_rect.top, (int)window_rect.bottom);
	// OutputDebugString("TEST");

	if (RegisterClassA(&window))
	{
		HWND window_handle = CreateWindowExA(
			0,
			window.lpszClassName,
			"Win32",
			WS_OVERLAPPEDWINDOW|WS_VISIBLE,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			window_rect.right-window_rect.left,
			window_rect.bottom-window_rect.top,
			0,
			0,
			hInstance,
			0);

		if (window_handle)
		{
			UpdateWindow(window_handle);

			running = true;
			while (running)
			{
				MSG message;
				BOOL message_result = GetMessageA(&message, 0, 0, 0);
				if (message_result > 0)
				{
					TranslateMessage(&message);
					DispatchMessageA(&message);
				}
				else
				{
					OutputDebugString("GetMessage returned 0");
					running = false;
				}

				if (gl_context)
				{
					update_and_render();
				}
			}
		}
		else
		{
			printf("CreateWindowEx failed\n");
		}
	}
	else
	{
		printf("RegisterClass failed\n");
	}

	return 0;
}

LRESULT CALLBACK window_callback (HWND hwnd,
	UINT message,
	WPARAM wParam,
	LPARAM lParam)
{
	LRESULT result = 0;

	switch (message)
	{
		case WM_CREATE:
		{
			hdc = GetDC(hwnd);

			PIXELFORMATDESCRIPTOR pixel_format_descriptor = {
				sizeof(PIXELFORMATDESCRIPTOR), // size
				1, // version
				PFD_SUPPORT_OPENGL|PFD_DRAW_TO_WINDOW|PFD_DOUBLEBUFFER,
				PFD_TYPE_RGBA, // color type
				16, // color depth
				0, 0, 0, 0, 0, 0, // color bits
				0, // no alpha buffer
				0, // alpha bits
				0, // no accumulation buffer
				0, 0, 0, 0, // accumulation bits
				16, // depth buffer
				0, // stencil buffer
				0, // stencil buffer
				PFD_MAIN_PLANE, // blarg
				0,
				0, 0, 0
			};
			int pixel_format = ChoosePixelFormat(hdc, &pixel_format_descriptor);
			if (!pixel_format)
			{
				printf("ChoosePixelFormat failed");
				exit(1);
			}
			if (SetPixelFormat(hdc, pixel_format, &pixel_format_descriptor) != TRUE)
			{
				printf("SetPixelFormat failed");
				exit(1);
			}

			LOGPALETTE *palette;
			int palette_size;
			if (pixel_format_descriptor.dwFlags&PFD_NEED_PALETTE)
			{
				palette_size = 1 << pixel_format_descriptor.cColorBits;

				palette = (LOGPALETTE*)malloc(sizeof(LOGPALETTE) + palette_size * sizeof(PALETTEENTRY));
				palette->palVersion = 0x300;
				palette->palNumEntries = palette_size;

				int red_mask = (1 << pixel_format_descriptor.cRedBits) - 1;
				int green_mask = (1 << pixel_format_descriptor.cGreenBits) - 1;
				int blue_mask = (1 << pixel_format_descriptor.cBlueBits) - 1;
				for (int i = 0; i < palette_size; i++)
				{
					palette->palPalEntry[i].peRed =
					(((i >> pixel_format_descriptor.cRedShift) & red_mask) * 255) / red_mask;
					palette->palPalEntry[i].peGreen =
					(((i >> pixel_format_descriptor.cGreenShift) & green_mask) * 255) / green_mask;
					palette->palPalEntry[i].peBlue =
					(((i >> pixel_format_descriptor.cBlueShift) & blue_mask) * 255) / blue_mask;
					palette->palPalEntry[i].peFlags = 0;
				}

				hpalette = CreatePalette(palette);
				free(palette);

				if (hpalette)
				{
					SelectPalette(hdc, hpalette, FALSE);
					RealizePalette(hdc);
				}

			}

			gl_context = wglCreateContext(hdc);
			wglMakeCurrent(hdc, gl_context);

			GLenum error = glewInit();
			if (error != GLEW_OK)
			{
				OutputDebugString("glewInit failed");
				exit(1);
			}

			init();

			const char *version = (const char*)glGetString(GL_VERSION);
			char string[64];
			sprintf(string, "OpenGL version %s \n", version);

			OutputDebugString(string);

			if (!gl_context)
			{
				exit(1);
			}

			glViewport(0, 0, 800, 600);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, 800, 600, 0, -10, 10);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glClearColor(1.0, 0.0, 1.0, 1.0);
		}
		break;
		case WM_SIZE:
		{
			printf("Window size message\n");
		}
		break;
		case WM_CLOSE:
		{
			printf("Window close message\n");
			running = false;
		}
		break;
		case WM_DESTROY:
		{
			printf("Window destroy message\n");
			running = false;
		}
		break;
		case WM_ACTIVATEAPP:
		{
			printf("Window activate message\n");
		}
		break;
		case WM_PAINT:
		{
			//PAINTSTRUCT paint;
			//BeginPaint(hwnd, &paint);

			//EndPaint(hwnd, &paint);
		}
		break;
		default:
		{
			result = DefWindowProc(hwnd, message, wParam, lParam);
		}
		break;
	}

	return result;
}

void update_and_render ()
{
	glClearColor(1.0, 0.0, 1.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);

	glColor4f(0.0, 1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
	glVertex3f(10.0, 10.0, 0);
	glVertex3f(100.0, 10.0, 0);
	glVertex3f(100.0, 100.0, 0);
	glVertex3f(10.0, 100.0, 0);
	glEnd();

	SwapBuffers(hdc);
}

void init ()
{
	//gl_gen_buffers = (GLGENBUFFERS*)wglGetProcAddress("glGenBuffers");
}