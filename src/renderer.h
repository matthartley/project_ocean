#ifndef _RENDERER_HEADER
#define _RENDERER_HEADER

struct renderer_buffer
{
	GLuint vertex_buffer;
	GLuint color_buffer;
	GLuint texcoord_buffer;
	GLuint index_buffer;
};

void renderer_init ();
void renderer_draw_rect (float x, float y, float width, float height,
	float r, float g, float b, float a);

struct renderer_buffer renderer_create_rect (int32 width, int32 height,
	float r, float g, float b, float a);

void renderer_draw_rect_buffer (struct renderer_buffer buffer,
	float x, float y, float r, float g, float b, float a);

#endif