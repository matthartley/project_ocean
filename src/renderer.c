#include "renderer.h"

void renderer_init ()
{
	glViewport(0, 0, 800, 600);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, 800, 600, 0, -10, 10);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(0.0, 0.0, 0.0, 1.0);
}

void renderer_draw_rect (float x, float y, float width, float height,
	float r, float g, float b, float a)
{
	glColor4f(r, g, b, a);
	glDisable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);

	glVertex3f(x, y, 0);
	glVertex3f(x+width, y, 0);
	glVertex3f(x+width, y+height, 0);
	glVertex3f(x, y+height, 0);

	glEnd();
}

struct renderer_buffer renderer_create_rect (int32 width, int32 height,
	float r, float g, float b, float a)
{
	float vertices[] = {
		-width/2, -height/2, 0.0,
		width/2, -height/2, 0.0,
		width/2, height/2, 0.0,
		-width/2, height/2, 0.0
	};

	float colors[] = {
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a
	};

	int indices[] = {
		0, 1, 2, 0, 2, 3
	};

	struct renderer_buffer buffer = {};

	glGenBuffers(1, &buffer.vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer.vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glGenBuffers(1, &buffer.color_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer.color_buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

	glGenBuffers(1, &buffer.index_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	return buffer;
}

void renderer_draw_rect_buffer (struct renderer_buffer buffer,
	float x, float y, float r, float g, float b, float a)
{
	glColor4f(r, g, b, a);
	glDisable(GL_TEXTURE_2D);

	glPushMatrix();
	glTranslatef(x, y, 0);

	glEnableClientState(GL_VERTEX_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, buffer.vertex_buffer);
	glVertexPointer(3, GL_FLOAT, 0, 0);

	// glEnableClientState(GL_COLOR_ARRAY);
	// glBindBuffer(GL_ARRAY_BUFFER, buffer.color_buffer);
	// glVertexPointer(4, GL_FLOAT, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.index_buffer);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glPopMatrix();
}