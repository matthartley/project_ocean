#ifndef _LINUX_HEADER
#define _LINUX_HEADER

typedef int8_t int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef uint8_t uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;
typedef float float32;
typedef double float64;

Display *display;
Window root;
GLint attributes[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
XVisualInfo *visual_info;
Colormap color_map;
XSetWindowAttributes set_window_attributes;
Window window;
GLXContext gl_context;
XWindowAttributes window_attributes;
XEvent event;
uint32 running = 1;

int main ();
void set_key (int keycode, int state);

#endif